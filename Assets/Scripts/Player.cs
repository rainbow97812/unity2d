﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Movement
{
    public Rigidbody2D playerPlayerRigid;
    public float playerSpeedScale = 2.0f;
    public float playerJumpScale = 10.0f;

    public float playerMaxSpeed = 10.0f;
    public int playerMaxHealth = 3;

    [System.NonSerialized] public bool playerIsPlayAvaliable = true;
    [System.NonSerialized] public bool playerIsMovementAvaliable = true;
    [System.NonSerialized] public bool playerIsDead = false;

    public float camDepth = 20.0f;

    private GameObject mainCam;
    private Vector3 camPos;

    GameObject OPTION_MENU;

    // Start is called before the first frame update
    protected override void Awake()
    {
        base.Awake();

        playerPlayerRigid = GetComponent<Rigidbody2D>();

        playerRigid = playerPlayerRigid;

        speedScale = playerSpeedScale;
        jumpScale = playerJumpScale;

        maxSpeed = playerMaxSpeed;
        maxHealth = playerMaxHealth;

        isPlayAvaliable = playerIsPlayAvaliable;
        isMovementAvaliable = playerIsMovementAvaliable;
        isDead = playerIsDead;

        camDepth = 20.0f;

        mainCam = GameObject.Find("Main Camera");
        camPos = Vector3.zero;

        OPTION_MENU = GameObject.Find("OPTION");
    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();

        camPos = transform.position;
        camPos += Vector3.right * 5.0f + Vector3.up * 2.0f + Vector3.back * camDepth;

        mainCam.transform.position = camPos;

        if(transform.position.y < -10.0)
        {
            Application.LoadLevel(Application.loadedLevelName);
        }
    }

    protected override void OnCollisionEnter2D(Collision2D col)
    {
        base.OnCollisionEnter2D(col);
    }

    protected override void OnGUI()
    {
        base.OnGUI();
        GUI.skin.button.fontSize = 40;
        if (GUI.Button(new Rect(30, 150, 300, 100), "MENU"))
        {
            Instantiate(OPTION_MENU);
        }

        if (GUI.Button(new Rect(30, 30, 300, 100), "RESET"))
        {
            Application.LoadLevel(Application.loadedLevel);
        }
    }

    protected override bool PlayerLife(int maxHealth, int health)
    {
        return base.PlayerLife(maxHealth, health);
    }

    protected override void Dead()
    {
        base.Dead();
    }
}

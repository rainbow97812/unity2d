﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [System.NonSerialized] public Rigidbody2D playerRigid;
    [System.NonSerialized] public float speedScale;
    [System.NonSerialized] public float jumpScale;

    [System.NonSerialized] public float maxSpeed;
    [System.NonSerialized] public int maxHealth;
    [System.NonSerialized] public int health;

    [System.NonSerialized] public bool isGround;
    [System.NonSerialized] public bool isPlayAvaliable;
    [System.NonSerialized] public bool isMovementAvaliable;
    [System.NonSerialized] public bool isDead;

    private Vector2 velocity;
    TouchHandler touchHand;

    string res;

    // Start is called before the first frame update
    protected virtual void Awake()
    {
        playerRigid = null;
        speedScale = 3.0f;
        jumpScale = 10.0f;

        maxSpeed = 10.0f;
        maxHealth = 3;

        health = maxHealth;

        isGround = false;
        isPlayAvaliable = false;
        isMovementAvaliable = false;
        isDead = false;

        velocity = Vector2.zero;
        touchHand = new TouchHandler();
    }

    protected virtual void FixedUpdate()
    {
        if (isDead == false && isMovementAvaliable && isPlayAvaliable)
        {
            TouchHandler.HPOS hTouch = touchHand.getTouchHPos();
            res = hTouch.ToString() + "\n";

            if (hTouch == TouchHandler.HPOS.LEFT)
            {
                if (isGround)
                    velocity += Vector2.left * speedScale;
                else if (!isGround) 
                    velocity = Vector2.left * maxSpeed;
            }
            else if (hTouch == TouchHandler.HPOS.RIGHT)
            {
                if (isGround)
                    velocity += Vector2.right * speedScale;
                else if (!isGround)
                    velocity = Vector2.right * maxSpeed;
            }
            else
            {
                velocity.x = 0.0f;
            }

            TouchHandler.VPOS vSlide = touchHand.getSlideVPos();
            res += vSlide.ToString() + "\n";

            if (vSlide == TouchHandler.VPOS.UP && isGround)
            {
                velocity += Vector2.up * jumpScale;
            }
            else if (vSlide == TouchHandler.VPOS.DOWN)
            {
                velocity.y = playerRigid.velocity.y;
            }
            else
            {
                velocity.y = playerRigid.velocity.y;
            }

            Vector2 adaptV = velocity;
            adaptV.x = Mathf.Clamp(adaptV.x, -maxSpeed, maxSpeed);

            playerRigid.velocity = velocity;
        }
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        //touchHand.report();
    }

    protected virtual void OnCollisionEnter2D(Collision2D col){}

    protected virtual void OnGUI(){}

    protected virtual bool PlayerLife(int maxHealth, int health)
    {
        this.maxHealth = maxHealth;
        this.health = health;

        return (this.health > 0);
    }

    protected virtual void Dead()
    {
        this.isMovementAvaliable = false;
        this.isDead = true;
    }
}

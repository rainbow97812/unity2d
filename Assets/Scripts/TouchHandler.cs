﻿using UnityEngine;

public class TouchHandler
{
    public static Vector2 preTouchH, touchH;
    public static Vector2 preTouchV, touchV;

    private int sensitivityCount = 5;
    //값이 클 수록 슬라이딩을 빠르게 해야 인식이 됩니다.
    //값이 너무 작으면 슬라이딩이 아닌 터치만으로도 결과가 도출
    private int minSlideScale = 30;

    private static int countH, countV; 

    public enum VPOS {DOWN = -1, CENTER = 0, UP = 1 , NT = 2};
    public enum HPOS {LEFT = -1, CENTER = 0, RIGHT = 1, NT = 2};
    public enum XYS {X = 0, Y = 1 };

    HPOS keepDownH;
    VPOS keepDownV;

    public TouchHandler()
    { 
        preTouchH = Vector2.zero;
        preTouchV = Vector2.zero;

        touchH = Vector2.zero;
        touchV = Vector2.zero;

        countH = 0;
        countV = 0;

        keepDownH = HPOS.NT;
        keepDownV = VPOS.NT;
    }

    public void report()
    {}

    public HPOS getTouchDownHPos()
    {
        int halfH = Screen.width / 2;

        Touch touch = Input.GetTouch(0);
        if (touch.tapCount == 0)
            return HPOS.NT;

        if(touch.phase == TouchPhase.Began)
            touchH = touch.position;

        if (touchH.x < halfH && touchH.x != 0.0)
        {
            touchH = Vector2.zero;
            return HPOS.LEFT;
        }
        else if (touchH.x > halfH)
        {
            touchH = Vector2.zero;
            return HPOS.RIGHT;
        }

        touchH = Vector2.zero;
        return HPOS.CENTER;
    }

    public VPOS getTouchDownVPos()
    {
        int halfV = Screen.height / 2;

        Touch touch = Input.GetTouch(0);
        if (touch.tapCount == 0)
            return VPOS.NT;
        
        if(touch.phase == TouchPhase.Began)
            touchV = touch.position;

        if (touchV.y < halfV && touchV.y != 0.0)
        {
            touchV = Vector2.zero;
            return VPOS.DOWN;
        }
        else if (touchV.y > halfV)
        {
            touchV = Vector2.zero;
            return VPOS.UP;
        }

        touchV = Vector2.zero;
        return VPOS.CENTER;
    }

    public HPOS getTouchHPos()
    {
        int halfH = Screen.width / 2;

        Touch touch = Input.GetTouch(0);
        if (!isKeepTouch(touch))
        {
            keepDownH = HPOS.NT;
            return HPOS.NT;
        }
        Debug.Log(touch.phase.ToString());

        touchH = touch.position;

        if (touchH.x < halfH && touchH.x != 0.0)
        {
            touchH = Vector2.zero;
            keepDownH = HPOS.LEFT;
            return HPOS.LEFT;
        }
        else if (touchH.x > halfH)
        {
            touchH = Vector2.zero;
            keepDownH = HPOS.RIGHT;
            return HPOS.RIGHT;
        }

        if (touch.phase == TouchPhase.Ended)
            keepDownH = HPOS.NT;

        touchH = Vector2.zero;
        return keepDownH;
    }

    public VPOS getTouchVPos()
    {
        int halfV = Screen.height / 2;

        Touch touch = Input.GetTouch(0);
        if (touch.tapCount == 0)
            return VPOS.NT;

        if (touch.phase == TouchPhase.Began)
            touchV = touch.position;

        if (touchV.y < halfV && touchV.y != 0.0)
        {
            touchV = Vector2.zero;
            keepDownV = VPOS.DOWN;
            return VPOS.DOWN;
        }
        else if (touchV.y > halfV)
        {
            touchV = Vector2.zero;
            keepDownV = VPOS.UP;
            return VPOS.UP;
        }

        if (touch.phase == TouchPhase.Ended)
            keepDownV = VPOS.NT;

        touchV = Vector2.zero;
        return keepDownV;
    }

    public HPOS getSlideHPos()
    {
        Touch t = Input.GetTouch(0);

        //터치가 시작되었을 시점
        //터치가 눌려졌을 시에 또는 preTouchH값이 존재 할 시에
        //시작 포인트와 루틴 포인트를 둘 다 허용한다
        if (countH == 0 && (isTouchDown(t) || isKeepTouch(t)))
        {
            preTouchH = t.position;
            countH++;
        }

        //시작 된 이후 부터 시점
        else if(0 < countH && countH < sensitivityCount)
        { 
            //도중에 끝낸다면
            if(t.phase == TouchPhase.Ended)
            {
                countH = 0;
                preTouchH = Vector2.zero;

                keepDownH = HPOS.NT;
                return HPOS.NT;
            }
            //도중에 끝내지 않는다면
            countH++;
        }

        else if(countH == sensitivityCount)
        {
            //루틴 계속해서 결과를 계산한다
            countH = 0;
            touchH = t.position;

            if (preTouchH.x < (touchH.x - minSlideScale))
            {
                preTouchH = Vector2.zero;
                touchH = Vector2.zero;
                //right
                keepDownH = HPOS.RIGHT;
                return HPOS.RIGHT;
            }
            else if (preTouchH.x > (touchH.x + minSlideScale))
            {
                preTouchH = Vector2.zero;
                touchH = Vector2.zero;
                //left
                keepDownH = HPOS.LEFT;
                return HPOS.LEFT;
            }
        }

        //return HPOS.CENTER;
        return keepDownH;
    }

    public VPOS getSlideVPos()
    {
         Touch t = Input.GetTouch(0);

        //터치가 시작되었을 시점
        //터치가 눌려졌을 시에 또는 preTouchV값이 존재 할 시에
        //시작 포인트와 루틴 포인트를 둘 다 허용한다
        if (countV == 0)
        {
            preTouchV = t.position;
            countV++;
        }

        //시작 된 이후 부터 시점
        else if(0 < countV && countV < sensitivityCount)
        { 
            //도중에 끝낸다면
            if(t.phase == TouchPhase.Ended)
            {
                countV = 0;
                preTouchV = Vector2.zero;

                keepDownV = VPOS.NT;
                return VPOS.NT;
            }
            //도중에 끝내지 않는다면
            countV++;
        }

        else if(countV == sensitivityCount)
        {
            //루틴 계속해서 결과를 계산한다
            countV = 0;
            touchV = t.position;

            if (preTouchV.y < (touchV.y - minSlideScale))
            {
                preTouchV = Vector2.zero;
                touchV = Vector2.zero;
                //right
                keepDownV = VPOS.UP;
                return VPOS.UP;
            }
            else if (preTouchV.y > (touchV.y + minSlideScale))
            {
                preTouchV = Vector2.zero;
                touchV = Vector2.zero;
                //left
                keepDownV = VPOS.DOWN;
                return VPOS.DOWN;
            }
        }

        //return VPOS.CENTER;
        return keepDownV;
    }

    //touchUP, touchDown
    //isTouchUP, isTouchDown
    //Keep

    bool isTouchDown(Touch t)
    {
        return (t.phase == TouchPhase.Began);
    }

    bool isKeepTouch(Touch t)
    {
        return (t.phase == TouchPhase.Stationary || t.phase == TouchPhase.Moved);
    }

    bool isTouchUp(Touch t)
    {
        return (t.phase == TouchPhase.Ended);
    }

    //get oneAxis that more bigger length.
    //X false Y true
    XYS getSelectAxis(Vector2 pre, Vector2 n)
    {
        float x = Mathf.Abs(n.x - pre.x);
        float y = Mathf.Abs(n.y - pre.y);

        if (x < y)
            return XYS.Y;
        return XYS.X;
    }
}
